/*
    SPDX-FileCopyrightText: 2020 Andrey Butirsky <butirsky@gmail.com>
    SPDX-License-Identifier: GPL-2.0-or-later
*/

import QtQuick 2.12
import Qt.labs.platform 1.1
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.workspace.components 2.0

Item {
    id: root

    // Загрузка шрифтов (начало) ----------------------------------------------
    FontLoader
    {
        id: mtsSansRegular;
        source: "fonts/MTS Sans/MTSSans-Regular.otf"
    }

    FontLoader
    {
        id: mtsSansCompact;
        source: "fonts/MTS Compact/MTSCompact-Regular.otf"
    }
    // Загрузка шрифтов (конец) -----------------------------------------------

    signal layoutSelected(int layout)

    Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation

    Plasmoid.compactRepresentation: KeyboardLayoutSwitcher {
        hoverEnabled: true

        Plasmoid.toolTipSubText: layoutNames.longName
        Plasmoid.status: hasMultipleKeyboardLayouts ? PlasmaCore.Types.ActiveStatus : PlasmaCore.Types.HiddenStatus

        Connections {
            target: keyboardLayout

            function onLayoutsListChanged() {
                plasmoid.clearActions()

                keyboardLayout.layoutsList.forEach(
                            function(layout, index) {
                                plasmoid.setAction(
                                            index,
                                            layout.longName,
                                            iconURL(layout.shortName).toString().substring(7) // remove file:// scheme
                                            )
                            }
                            )
            }

            function onLayoutChanged() {
                root.Plasmoid.activated()
            }
        }

        Rectangle
        {
            id: whiteBackground
            x: (parent.width - width)/2
            y: ((parent.height - height)/2)
            width: 24
            height: 24
            color: "white"
            radius: 4
        }

        Connections {
            target: root

            function onLayoutSelected(layout) {
               keyboardLayout.layout = layout
            }
        }

        PlasmaCore.IconItem {
            id: flag

            anchors.fill: parent
            visible: valid && (Plasmoid.configuration.displayStyle === 1 || Plasmoid.configuration.displayStyle === 2)
            active: containsMouse
            source: iconURL(layoutNames.shortName)

            BadgeOverlay {
                id: badgeLayout
                anchors.bottom: parent.bottom
                anchors.right: parent.right

                visible: !countryCode.visible && Plasmoid.configuration.displayStyle === 2

                text: countryCode.text
                icon: flag
            }
        }

        PlasmaComponents3.Label {
            id: countryCode
            anchors.fill: whiteBackground
            visible: Plasmoid.configuration.displayStyle === 0 || !flag.valid
            color:"#111111"
            font.pixelSize: 14
            font.family: mtsSansCompact.name
            fontSizeMode: Text.Fit
            font.weight: Font.DemiBold
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: layoutNames.displayName || layoutNames.shortName
            font.capitalization: Font.AllUppercase
        }
    }

    function iconURL(name) {
        return StandardPaths.locate(StandardPaths.GenericDataLocation,
                        "kf5/locale/countries/" + name + "/flag.png")
    }

    function actionTriggered(selectedLayout) {
        layoutSelected(selectedLayout)
    }
}
